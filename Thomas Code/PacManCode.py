# https://chat.openai.com/share/4acfc0e9-c10a-4190-9829-852a5b7b5b70
# This the code I developed using ChatGPT. This code was then used along with Quinn and Nico's code to create the final code.

import os
import random

def clear_screen():
    os.system('cls' if os.name == 'nt' else 'clear')

def print_grid(grid, pacman, ghosts):
    for i, row in enumerate(grid):
        if i == pacman[0]:
            row[pacman[1]] = 'P'
        for ghost in ghosts:
            if i == ghost[0]:
                row[ghost[1]] = 'G'
        print("".join(row))

def create_grid():
    return [[" " for _ in range(20)] for _ in range(10)]

def move_pacman(grid, pacman, direction):
    row, col = pacman
    if direction == 'w':
        row -= 1
    elif direction == 's':
        row += 1
    elif direction == 'a':
        col -= 1
    elif direction == 'd':
        col += 1

    if 0 <= row < len(grid) and 0 <= col < len(grid[0]) and grid[row][col] != '#':
        grid[pacman[0]][pacman[1]] = ' '  # Clear previous position
        return row, col
    return pacman

def move_ghosts(grid, ghosts):
    for i, ghost in enumerate(ghosts):
        row, col = ghost
        directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        random.shuffle(directions)
        for dr, dc in directions:
            if 0 <= row + dr < len(grid) and 0 <= col + dc < len(grid[0]) and grid[row + dr][col + dc] != '#':
                grid[ghost[0]][ghost[1]] = ' '  # Clear previous position
                ghosts[i] = (row + dr, col + dc)
                break

def create_coins(grid):
    coins = []
    for _ in range(10):
        row, col = random.randint(0, 9), random.randint(0, 19)
        while grid[row][col] != " ":
            row, col = random.randint(0, 9), random.randint(0, 19)
        grid[row][col] = "C"
        coins.append((row, col))
    return coins

def is_game_won(coins):
    return not coins  # Game is won when all coins are collected

def is_game_over(pacman, ghosts):
    return pacman in ghosts

def main():
    grid = create_grid()
    pacman = [1, 1]
    ghosts = [[5, 5], [3, 8], [7, 12], [4, 16]]  # Four ghosts
    coins = create_coins(grid)
    direction = ''

    while True:
        clear_screen()
        print_grid(grid, pacman, ghosts)
        print("Use 'w', 'a', 's', 'd' to move. Press 'q' to quit.")

        direction = input("Enter your move: ").lower()

        if direction in ('w', 'a', 's', 'd'):
            pacman = move_pacman(grid, pacman, direction)
            move_ghosts(grid, ghosts)

            if pacman in coins:
                coins.remove(pacman)
                grid[pacman[0]][pacman[1]] = ' '  # Remove collected coin

            if is_game_over(pacman, ghosts):
                clear_screen()
                print("Game Over! You were caught by a ghost!")
                break

            if is_game_won(coins):
                clear_screen()
                print("Congratulations! You collected all the coins and won the game!")
                break
        elif direction == 'q':
            break

if __name__ == "__main__":
    main()