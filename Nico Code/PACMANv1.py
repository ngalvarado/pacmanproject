# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 09:16:09 2023

@author: loslo
"""
import pygame
import random

# Initialize Pygame
pygame.init()

# Constants
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
PACMAN_SIZE = 30
PACMAN_SPEED = 5
GHOST_SPEED = 3

# Colors
BLACK = (0, 0, 0)
YELLOW = (255, 255, 0)
RED = (255, 0, 0)

# Create the screen
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Pac-Man")

# Pac-Man
pacman_x = SCREEN_WIDTH // 2
pacman_y = SCREEN_HEIGHT // 2
pacman_direction = 0  # 0: right, 1: down, 2: left, 3: up

# Ghost
ghost_x = random.randint(0, SCREEN_WIDTH - PACMAN_SIZE)
ghost_y = random.randint(0, SCREEN_HEIGHT - PACMAN_SIZE)
ghost_direction = random.choice([0, 1, 2, 3])

# Game loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    keys = pygame.key.get_pressed()
    if keys[pygame.K_RIGHT]:
        pacman_direction = 0
    elif keys[pygame.K_DOWN]:
        pacman_direction = 1
    elif keys[pygame.K_LEFT]:
        pacman_direction = 2
    elif keys[pygame.K_UP]:
        pacman_direction = 3

    # Move Pac-Man
    if pacman_direction == 0:
        pacman_x += PACMAN_SPEED
    elif pacman_direction == 1:
        pacman_y += PACMAN_SPEED
    elif pacman_direction == 2:
        pacman_x -= PACMAN_SPEED
    elif pacman_direction == 3:
        pacman_y -= PACMAN_SPEED

    # Wrap Pac-Man around the screen
    pacman_x %= SCREEN_WIDTH
    pacman_y %= SCREEN_HEIGHT

    # Move Ghost
    if ghost_direction == 0:
        ghost_x += GHOST_SPEED
    elif ghost_direction == 1:
        ghost_y += GHOST_SPEED
    elif ghost_direction == 2:
        ghost_x -= GHOST_SPEED
    elif ghost_direction == 3:
        ghost_y -= GHOST_SPEED

    # Wrap Ghost around the screen
    ghost_x %= SCREEN_WIDTH
    ghost_y %= SCREEN_HEIGHT

    # Draw everything
    screen.fill(BLACK)
    pygame.draw.circle(screen, YELLOW, (pacman_x, pacman_y), PACMAN_SIZE // 2)
    pygame.draw.rect(screen, RED, (ghost_x, ghost_y, PACMAN_SIZE, PACMAN_SIZE))

    pygame.display.flip()

# Quit the game
pygame.quit()
