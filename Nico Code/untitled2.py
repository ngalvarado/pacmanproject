# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 09:41:08 2023

@author: loslo
"""

import random

# Constants
WIDTH = 20
HEIGHT = 20
PACMAN = 'P'
GHOST = 'G'
GHOST2 = 'g'
COIN = 'C'
EMPTY = ' '
# Initialize the game board
board = [[EMPTY for _ in range(WIDTH)] for _ in range(HEIGHT)]

# Place Pac-Man on the board
pacman_x = random.randint(0, WIDTH - 1)
pacman_y = random.randint(0, HEIGHT - 1)
board[pacman_y][pacman_x] = PACMAN

# Place a ghost on the board

num_coin = 10
for _ in range(num_coin):
    while True:
        coin_x = random.randint(0, WIDTH - 1)
        coin_y = random.randint(0, HEIGHT - 1)
        if board[coin_y][coin_x] == EMPTY:
            board[coin_y][coin_x] = COIN
            break

# Place some coins on the board
ghost_x = random.randint(0, WIDTH - 1)
ghost_y = random.randint(0, HEIGHT - 1)
board[ghost_y][ghost_x] = GHOST

ghost_x2 = random.randint(0, WIDTH - 1)
ghost_y2 = random.randint(0, HEIGHT - 1)
board[ghost_y][ghost_x] = GHOST2

# Game loop
while True:
    # Display the game board
    for row in board:
        print(' '.join(row))
    print()

    # Check if Pac-Man and Ghost are in the same position
    if pacman_x == ghost_x and pacman_y == ghost_y:
        print("Game Over! You were caught by the ghost.")
        break
    if pacman_x == ghost_x2 and pacman_y == ghost_y2:
        print("Game Over! You were caught by the ghost.")
        break

    # Check if all coins are collected
    if all(EMPTY not in row for row in board):
        print("Congratulations! You collected all the coins and won!")
        break

    # Get user input
    move = input("Enter a direction (W/A/S/D to move, Q to quit): ").lower()

    # Move Pac-Man
    if move == 'w' and pacman_y > 0:
        board[pacman_y][pacman_x] = EMPTY
        pacman_y -= 1
    elif move == 'a' and pacman_x > 0:
        board[pacman_y][pacman_x] = EMPTY
        pacman_x -= 1
    elif move == 's' and pacman_y < HEIGHT - 1:
        board[pacman_y][pacman_x] = EMPTY
        pacman_y += 1
    elif move == 'd' and pacman_x < WIDTH - 1:
        board[pacman_y][pacman_x] = EMPTY
        pacman_x += 1
    elif move == 'q':
        print("Thanks for playing!")
        break

    # Update Pac-Man's position
    board[pacman_y][pacman_x] = PACMAN

    # Move the ghost randomly
    board[ghost_y][ghost_x] = EMPTY
    ghost_x = random.randint(0, WIDTH - 1)
    ghost_y = random.randint(0, HEIGHT - 1)
    board[ghost_y][ghost_x] = GHOST
    
    board[ghost_y2][ghost_x2] = EMPTY
    ghost_x2 = random.randint(0, WIDTH - 1)
    ghost_y2 = random.randint(0, HEIGHT - 1)
    board[ghost_y2][ghost_x2] = GHOST2
