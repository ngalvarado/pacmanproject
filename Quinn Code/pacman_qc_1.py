#new variable for top and bottom rows of game map
#remove period from game map easier to play
#most advanced game version right now

# author: Quinn Cunningham @qjcunningham
# cite: https://chat.openai.com/share/122aaff8-1f77-4669-b164-3acb4826edb9
# cite: https://chat.openai.com/share/c39c8d7a-d1a7-4487-b42a-0352ee9248bb

import os
import random
import sys

# Constants
WIDTH, HEIGHT = 15, 15
PACMAN = 'P'
COIN = 'C'
WALL = '-|'  # You can use multiple symbols for walls
GHOST = 'G'
SPACE = ' '

# Game state
game_map = [
    "-----------------",
    "|               |",
    "| --------- --| |",
    "| |           | |",
    "|   -------   | |",
    "| | |     | | | |",
    "| |   | | | | | |",
    "| | | |   | | | |",
    "| | |   | |     |",
    "|   | | | | | | |",
    "| | | | | | | | |",
    "| |           | |",
    "| ------ ---- | |",
    "|               |",
    "-----------------"
]
pacman_x, pacman_y = 7, 7
ghosts = [
    {'x': 2, 'y': 2},
    {'x': 12, 'y': 2},
    {'x': 2, 'y': 12},
    {'x': 12, 'y': 12}
]
total_coins = 10
collected_coins = 0

def initialize_game():
    global game_map, pacman_x, pacman_y, ghosts, collected_coins
    game_map = [
    "-----------------",
    "|               |",
    "| --------- --| |",
    "| |           | |",
    "|   -------   | |",
    "| | |     | | | |",
    "| |   | | | | | |",
    "| | | |   | | | |",
    "| | |   | |     |",
    "|   | | | | | | |",
    "| | | | | | | | |",
    "| |           | |",
    "| ------ ---- | |",
    "|               |",
    "-----------------"
    ]
    pacman_x, pacman_y = 7, 7
    ghosts = [
        {'x': 2, 'y': 2},
        {'x': 12, 'y': 2},
        {'x': 2, 'y': 12},
        {'x': 12, 'y': 12}
    ]
    collected_coins = 0

def place_coins():
    coin_positions = []
    while len(coin_positions) < total_coins:
        y, x = random.randint(0, HEIGHT - 1), random.randint(0, WIDTH - 1)
        if game_map[y][x] == SPACE and (y, x) not in coin_positions:
            game_map[y] = game_map[y][:x] + COIN + game_map[y][x + 1:]
            coin_positions.append((y, x))

def are_coins_left():
    for row in game_map:
        if COIN in row:
            return True
    return False

def print_game():
    os.system('cls' if os.name == 'nt' else 'clear')
    for row in game_map:
        print(row)
    print("Collected Coins: {}".format(collected_coins))

def move_pacman(x, y):
    global pacman_x, pacman_y, collected_coins
    if WALL[0] in game_map[y][x] or WALL[1] in game_map[y][x]:
        return
    if game_map[y][x] != WALL[0] and game_map[y][x] != WALL[1]:
        if game_map[y][x] == COIN:
            collected_coins += 1
        game_map[pacman_y] = game_map[pacman_y][:pacman_x] + SPACE + game_map[pacman_y][pacman_x + 1:]
        pacman_x, pacman_y = x, y
        game_map[pacman_y] = game_map[pacman_y][:pacman_x] + PACMAN + game_map[pacman_y][pacman_x + 1:]

def move_ghost(ghost):
    directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    random.shuffle(directions)
    for dx, dy in directions:
        new_x = ghost['x'] + dx
        new_y = ghost['y'] + dy
        if WALL[0] not in game_map[new_y][new_x] and WALL[1] not in game_map[new_y][new_x] and game_map[new_y][new_x] != GHOST:
            game_map[ghost['y']] = game_map[ghost['y']][:ghost['x']] + SPACE + game_map[ghost['y']][ghost['x'] + 1:]
            ghost['x'], ghost['y'] = new_x, new_y
            game_map[ghost['y']] = game_map[ghost['y']][:ghost['x']] + GHOST + game_map[ghost['y']][ghost['x'] + 1:]
            break

# Initialize the game and place coins
initialize_game()
place_coins()

# Main game loop
while True:
    print_game()
    print("Use WASD to move Pac-Man")
    move = input()
    if move.lower() == 'w':
        move_pacman(pacman_x, pacman_y - 1)
    elif move.lower() == 's':
        move_pacman(pacman_x, pacman_y + 1)
    elif move.lower() == 'a':
        move_pacman(pacman_x - 1, pacman_y)
    elif move.lower() == 'd':
        move_pacman(pacman_x + 1, pacman_y)

    # Move ghosts
    for ghost in ghosts:
        move_ghost(ghost)

    # Check if Pac-Man is caught by a ghost
    for ghost in ghosts:
        if ghost['x'] == pacman_x and ghost['y'] == pacman_y:
            print_game()
            print("Pac-Man was caught by a ghost. Game over!")
            sys.exit()

    # Check if all coins are collected
    if collected_coins == total_coins or not are_coins_left():
        print_game()
        print("Congratulations! You collected all the coins and escaped the ghosts!")
        sys.exit()