import random


# Constants
WIDTH = 20 ## Size of board
HEIGHT = 20 # Other size of board
PACMAN = 'P' # P on board is pacman
GHOST = 'G' # G on board is a Ghost
GHOST2 = 'g' # g on board is another ghost
COIN = 'C' # C on board is Coin
EMPTY = '.' # . on board makes the grid
game_over = False 
# Initialize the game board
board = [[EMPTY for _ in range(WIDTH)] for _ in range(HEIGHT)]

# Place Pac-Man on the board
pacman_x = random.randint(0, WIDTH - 1)
pacman_y = random.randint(0, HEIGHT - 1)
board[pacman_y][pacman_x] = PACMAN

def reset_game():
    '''
    param: None
    Resets game if function is called
    Places Pac Man on board
    Arranges coins on board. Depending upon number of coins given
    Places ghosts on board
    Returns nothing
    '''
    global pacman_x, pacman_y, game_over, board

    pacman_x = random.randint(0, WIDTH - 1)
    pacman_y = random.randint(0, HEIGHT - 1)
    game_over = False

    board = [[EMPTY for _ in range(WIDTH)] for _ in range(HEIGHT)]
    board[pacman_y][pacman_x] = PACMAN

    for _ in range(num_coin):
        while True:
            coin_x = random.randint(0, WIDTH - 1)
            coin_y = random.randint(0, HEIGHT - 1)
            if board[coin_y][coin_x] == EMPTY:
                board[coin_y][coin_x] = COIN
                break
            
    #Places ghosts on the board during reset

    ghost_x = random.randint(0, WIDTH - 1)
    ghost_y = random.randint(0, HEIGHT - 1)
    board[ghost_y][ghost_x] = GHOST

    ghost_x2 = random.randint(0, WIDTH - 1)
    ghost_y2 = random.randint(0, HEIGHT - 1)
    board[ghost_y2][ghost_x2] = GHOST2

# Place a ghost on the board



num_coin = 10
# Place some coins on the board
for _ in range(num_coin):
    while True:
        coin_x = random.randint(0, WIDTH - 1)
        coin_y = random.randint(0, HEIGHT - 1)
        if board[coin_y][coin_x] == EMPTY:
            board[coin_y][coin_x] = COIN
            break

# Places ghosts on the board
ghost_x = random.randint(0, WIDTH - 1)
ghost_y = random.randint(0, HEIGHT - 1)
board[ghost_y][ghost_x] = GHOST

ghost_x2 = random.randint(0, WIDTH - 1)
ghost_y2 = random.randint(0, HEIGHT - 1)
board[ghost_y][ghost_x] = GHOST2

# Game loop
while True:
    # Display the game board
    for row in board:
        print(' '.join(row))
    print()

    # Check if Pac-Man and Ghost are in the same position
    if pacman_x == ghost_x and pacman_y == ghost_y:
        print("Game Over! You were caught by the ghost.")
        break
    # Ends game if caught by ghost
    
    if pacman_x == ghost_x2 and pacman_y == ghost_y2:
        print("Game Over! You were caught by the ghost.")
        break
    # ends game if caught by other ghost

    # Check if all coins are collected
    if all(COIN not in row for row in board):
        print("Congratulations! You collected all the coins and won!")
        break

    # Get user input
    move = input("Enter a direction (W/A/S/D to move, R to Reset Game, Q to quit): ").lower()
    
    # Move Pac-Man with imput from keyboard
    if move == 'w' and pacman_y > 0:
        board[pacman_y][pacman_x] = EMPTY
        pacman_y -= 1
    elif move == 'a' and pacman_x > 0:
        board[pacman_y][pacman_x] = EMPTY
        pacman_x -= 1
    elif move == 's' and pacman_y < HEIGHT - 1:
        board[pacman_y][pacman_x] = EMPTY
        pacman_y += 1
    elif move == 'd' and pacman_x < WIDTH - 1:
        board[pacman_y][pacman_x] = EMPTY
        pacman_x += 1
        
    # resets game if r is pressed
    elif move == 'r':
        reset_game()
        
    # quits game is q is pressed
    elif move == 'q':
        print("Thanks for playing!")
        break

    # Update Pac-Man's position
    board[pacman_y][pacman_x] = PACMAN

    # Moves the ghosts randomly 
    board[ghost_y][ghost_x] = EMPTY
    ghost_x = random.randint(0, WIDTH - 1)
    ghost_y = random.randint(0, HEIGHT - 1)
    board[ghost_y][ghost_x] = GHOST
    
    board[ghost_y2][ghost_x2] = EMPTY
    ghost_x2 = random.randint(0, WIDTH - 1)
    ghost_y2 = random.randint(0, HEIGHT - 1)
    board[ghost_y2][ghost_x2] = GHOST2
    
    # Code we used to start the game
    # https://chat.openai.com/share/e648ae6f-9070-456e-992e-faaf3bbedafa
    
    # This code designs a PACMAN game. 
    # The game is simplified and doesn't include a maze.
    # To move PACMAN you use the a, s, w, d keys.
    # This code was created by merging Nico, Quinn, and Thomas' code.
    # Look at the console to watch the code work.
